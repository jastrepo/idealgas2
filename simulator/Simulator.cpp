//
// Created by nikita on 14.02.18.
//

//#define COLLIDE_CHECK

#include "Simulator.h"
#include <cmath>
#include <iostream>

using std::cout;
using std::min;
using std::endl;
using std::make_pair;

void Simulator::printError(pair<Object *, Object *> objects) {
  cout << "AAAA" << endl;
}

pair<Object *, Object *> Simulator::getObjects(int objectNum1, int objectNum2) {
#ifdef COLLIDE_CHECK
  if (objectNum1 == objectNum2 || objectNum1 < 0 || objectNum2 < 0 || objectNum1 > objects.size() ||
      objectNum2 > objects.size()) {
    cout << "Some error! i: " << objectNum1 << " j: " << objectNum2 << endl;
    return {nullptr, nullptr};
  }
#endif
  if (lastImpacted[objectNum1] == objectNum2 || lastImpacted[objectNum2] == objectNum1) {
    return {nullptr, nullptr};
  }
  return make_pair(objects[objectNum1], objects[objectNum2]);
}

bool Simulator::impactCircles(Circle *first, Circle *second) {
#ifdef COLLIDE_CHECK
  if (getDistanceBetweenCircles(first, second) - (first->getRadius() + second->getRadius()) < -0.001) {
    cout << "They are too far!!!";
    return true;
  }
#endif
  const auto &c = first->getPosition(), &v = first->getVelocity();
  const auto &c1 = second->getPosition(), &v1 = second->getVelocity();
  double x = v.x, y = v.y;
  double x1 = c1.x - c.x, y1 = c1.y - c.y;
  double x2 = v1.x - x, y2 = v1.y - y;
  double k = (x1 * x2 + y1 * y2) / (x1 * x1 + y1 * y1);
  double x3 = x1 * k, y3 = y1 * k;
  first->setVelocity({x3 + x, y3 + y});
  second->setVelocity({x2 - x3 + x, y2 - y3 + y});
  return false;
}

void Simulator::impact(pair<Object *, Object *> objects) {
  if (objects.first == nullptr) {
    return;
  }
  auto first = objects.first, second = objects.second;
  pair<int, int> type = {first->getType(), second->getType()};
  if (type.first > type.second) {
    type = {type.second, type.first};
    objects = {objects.second, objects.first};
    first = objects.first;
    second = objects.second;
  }
  bool res = false;
  if (type.first == Circle::CIRCLE) {
    auto secondType = type.second;
    auto firstObj = dynamic_cast<Circle *>(first);
    if (secondType == Circle::CIRCLE) {
      res = impactCircles(firstObj, dynamic_cast<Circle *>(second));
    } else if (secondType == LineSegment::SEGMENT) {
      res = impactCircleWithSegment(firstObj, dynamic_cast<LineSegment *>(second));
    }
  }
  if (res) {
    printError(objects);
  }
}

double Simulator::getDistanceBetweenSegmentAndPoint(LineSegment *segment, const Vector2 &point) {
  const auto &c2 = segment->getSecondVertex();
  const auto &c1 = segment->getPosition();
  const auto &dir = segment->getDirection();
  double x = c1.x - point.x, y = c1.y - point.y;
  double x1 = c2.x - point.x, y1 = c2.y - point.y;
  double x2 = dir.x, y2 = dir.y;
  if (x * x2 + y * y2 < 0) {
    if (x1 * x2 + y1 * y2 > 0) {
      return fabs(dir.y * point.x - dir.x * point.y + c2.x * c1.y - c2.y * c1.x) / sqrt(dir.squareLength());
    } else {
      return sqrt(x1 * x1 + y1 * y1);
    }
  } else {
    if (x1 * x2 + y1 * y2 > 0) {
      return sqrt(x2 * x2 + y2 * y2);
    } else {
      cout << "Undefined error";
      return -1;
    }
  }
}

double Simulator::getDistanceBetweenCircles(Circle *first, Circle *second) {
  return first->getPosition().getDistanceTo(second->getPosition());
}

double Simulator::getDistanceBetweenCircleAndSegment(Circle *circle, LineSegment *segment) {
  return getDistanceBetweenSegmentAndPoint(segment, circle->getPosition());
}

double Simulator::getDistance(pair<Object *, Object *> objects) {
  if (objects.first == nullptr) {
    return -1;
  }
  auto first = objects.first, second = objects.second;
  pair<int, int> type = {first->getType(), second->getType()};
  if (type.first > type.second) {
    type = {type.second, type.first};
    objects = {objects.second, objects.first};
    first = objects.first;
    second = objects.second;
  }
  double res = 0;
  if (type.first == Circle::CIRCLE) {
    auto secondType = type.second;
    auto firstObj = dynamic_cast<Circle *>(first);
    if (secondType == Circle::CIRCLE) {
      res = getDistanceBetweenCircles(firstObj, dynamic_cast<Circle *>(second));
    } else if (secondType == LineSegment::SEGMENT) {
      res = getDistanceBetweenCircleAndSegment(firstObj, dynamic_cast<LineSegment *>(second));
    }
  }
  if (res == -1) {
    printError(objects);
  }
  return res;
}

double Simulator::getTimeOfImpactCircles(Circle *first, Circle *second) {
  double r1 = first->getRadius(), r2 = second->getRadius();
  double r = r1 + r2;
#ifdef COLLIDE_CHECK
  if (getDistanceBetweenCircles(first, second) < r) {
    cout << "Coming through!!";
    return -2;
  }
#endif
  const auto &c = second->getPosition();
  const auto &v = second->getVelocity();
  const auto &c1 = first->getPosition();
  const auto &v1 = first->getVelocity();
  double x1 = c.x - c1.x, y1 = c.y - c1.y;
  double x2 = v.x - v1.x, y2 = v.y - v1.y;
  double t = x2 * y1 - y2 * x1;
  double sabs = x2 * x2 + y2 * y2;
  if (fabs(sabs) < 1e-10) {
    return -1; // Their relative speed equals 0
  }
  double D = r * r * sabs - t * t;
  if (D < 0) {
    return -1; // Their velocities are concurrent
  }
  double x = -(x1 * x2 + y1 * y2) / sabs;
  D = sqrt(D) / sabs;
  double t1 = x + D, t2 = x - D;
  if (t1 < 0) {
    if (t2 < 0) {
      return -1;
    } else {
      return t2;
    }
  } else {
    if (t2 < 0) {
      return t1;
    } else {
      return min(t1, t2);
    }
  }
}

double Simulator::getTimeOfImpactCircleWithSegment(Circle *circle, LineSegment *segment) {
  double r = circle->getRadius();
#ifdef COLLIDE_CHECK
  double dist = getDistanceBetweenCircleAndSegment(circle, segment);
  if (dist - r < -0.001) {
    cout << "Coming through!!!";
    return -2;
}
#endif
  const auto &c = circle->getPosition(), v = circle->getVelocity();
  double x = c.x, y = c.y;
  double x3 = v.x, y3 = v.y;
  const auto &c2 = segment->getSecondVertex();
  double time;
  {
    const auto &c1 = segment->getPosition();
    const auto &dir = segment->getDirection();
    double x1 = c1.x - x, y1 = c1.y - y;
    double x2 = c2.x - x, y2 = c2.y - y;
    double x4 = dir.x, y4 = dir.y;
    double vecProd = x3 * y4 - y3 * x4;
    if (fabs(vecProd) < 1e-10) {
      time = -1; // The velocity concurrent to the line
    } else {
      double D = r * sqrt(dir.squareLength()) / vecProd;
      double t = (x1 * y2 - x2 * y1) / vecProd;
      double t1 = t + D, t2 = t - D;
      if (t1 < 0) {
        if (t2 < 0) {
          time = -1;
        } else {
          time = t2;
        }
      } else {
        if (t2 < 0) {
          time = t1;
        } else {
          time = min(t1, t2);
        }
      }
    }
  }
  if (time < 0) {
    return time;
  }
  auto nextPos = c + v * time;
  if (abs(getDistanceBetweenSegmentAndPoint(segment, nextPos) - circle->getRadius()) < 0.001) {
    return time;
  } else {
    return -1;
  }
}

double Simulator::getTimeOfImpact(pair<Object *, Object *> objects) {
  if (objects.first == nullptr) {
    return -1;
  }
  auto first = objects.first, second = objects.second;
  pair<int, int> type = {first->getType(), second->getType()};
  if (type.first > type.second) {
    type = {type.second, type.first};
    objects = {objects.second, objects.first};
    first = objects.first;
    second = objects.second;
  }
  double res = -1;
  if (type.first == Circle::CIRCLE) {
    auto secondType = type.second;
    auto firstObj = dynamic_cast<Circle *>(first);
    if (secondType == Circle::CIRCLE) {
      res = getTimeOfImpactCircles(firstObj, dynamic_cast<Circle *>(second));
    } else if (secondType == LineSegment::SEGMENT) {
      res = getTimeOfImpactCircleWithSegment(firstObj, dynamic_cast<LineSegment *>(second));
    }
  }
  if (res < 0) {
    if (res == -2) {
      printError(objects);
    }
    return -1;
  } else {
    return res + time;
  }
}

void Simulator::calculateTimesOfImpactsForObject(int objectNum) {
  if (objectNum > 0) {
    updateImpactTimeForRow(objectNum);
    calculateMinForRow(objectNum);
  }
  if (objectNum < objects.size()) {
    for (int i = objectNum + 1; i < impactTimePoints.size(); i++) {
      updateImpactTime(i, objectNum);
      if (nearestImpactObjectInRow[i] == objectNum) {
        calculateMinForRow(i);
      } else {
        updateMin(impactTimePoints[i][objectNum], objectNum, nearestImpactInRow[i], nearestImpactObjectInRow[i]);
      }
    }
  }
}

void Simulator::calculateMinForRow(int objectNum) {
  nearestImpactInRow[objectNum] = -1;
  nearestImpactObjectInRow[objectNum] = -1;
  const auto &currentRow = impactTimePoints[objectNum];
  for (int i = 0; i < currentRow.size(); i++) {
    updateMin(currentRow[i], i, nearestImpactInRow[objectNum], nearestImpactObjectInRow[objectNum]);
  }
  updateMin(nearestImpactInRow[objectNum], objectNum, nearestImpact, nearestImpactRow);
}

void Simulator::calculateNearestImpact() {
  nearestImpact = nearestImpactRow = -1;
  for (int i = 0; i < nearestImpactInRow.size(); i++) {
    updateMin(nearestImpactInRow[i], i, nearestImpact, nearestImpactRow);
  }
}

void Simulator::updateMin(const double currentVal, const int currentPos, double &min, int &minPos) {
  if (currentVal != -1 && (min == -1 || min > currentVal)) {
    min = currentVal;
    minPos = currentPos;
  }
}

void Simulator::updateImpactTime(int i, int j) {
  impactTimePoints[i][j] = getTimeOfImpact(getObjects(i, j));
}

void Simulator::updateImpactTimeForRow(int objectNum) {
  const auto &currentRow = impactTimePoints[objectNum];
  for (int i = 0; i < currentRow.size(); i++) {
    updateImpactTime(objectNum, i);
  }
}

void Simulator::skipTimeTo(double timePoint) {
  double delta = timePoint - time;
  if (delta < -0.001) {
    cout << "Moving forward to past!!!!" << endl;
    return;
  }
  for (auto &object:objects) {
    object->setPosition(object->getPosition() + object->getVelocity() * delta);
  }
  time = timePoint;
}

void Simulator::checkIfCorrect() {
#ifdef COLLIDE_CHECK
  for (int i = 0; i < impactTimePoints.size(); i++) {
    for (int j = 0; j < impactTimePoints[i].size(); j++) {
      double currVal = impactTimePoints[i][j], realVal = getTimeOfImpact(getObjects(i, j));
      if (abs(currVal - realVal) > 0.001) {
        cout << "Great error x: " << i << " y:" << j << endl;
      }
    }
  }
#endif
}

void Simulator::update(double fdeltaTime) {}

void Simulator::simulateTo(double deltaTime) {
  double nextTimePoint = time + deltaTime;
  int x, y;
  while (nextTimePoint - nearestImpact > -0.001 && nearestImpact != -1) {
    x = nearestImpactRow;
    y = nearestImpactObjectInRow[x];
    skipTimeTo(nearestImpact);
    auto c_objects = getObjects(x, y);
    impact(c_objects);
    lastImpacted[x] = y;
    lastImpacted[y] = x;
    timeOfLastImpact[x] = timeOfLastImpact[y] = nearestImpact;
    calculateTimesOfImpactsForObject(x);
    calculateTimesOfImpactsForObject(y);
    calculateNearestImpact();
    if (c_objects.first != nullptr) {
      c_objects.first->impact(c_objects.second);
      c_objects.second->impact(c_objects.first);
    }
  }
  skipTimeTo(nextTimePoint);
  checkIfCorrect();
  for (auto &lastImpacted1:lastImpacted) {
    lastImpacted1 = -1;
  }
}

void Simulator::prepareSimulation() {
  nearestImpactInRow.resize(objects.size());
  fill(nearestImpactInRow.begin(), nearestImpactInRow.end(), -1);

  nearestImpactObjectInRow.resize(objects.size());
  fill(nearestImpactObjectInRow.begin(), nearestImpactObjectInRow.end(), -1);

  lastImpacted.resize(objects.size());
  fill(lastImpacted.begin(), lastImpacted.end(), -1);

  timeOfLastImpact.resize(objects.size());
  fill(timeOfLastImpact.begin(), timeOfLastImpact.end(), -1);

  impactTimePoints.resize(objects.size());
  for (int i = 0; i < objects.size(); i++) {
    impactTimePoints[i] = vector<double>(i, -1);
  }
  for (int i = 0; i < impactTimePoints.size(); i++) {
    for (int j = 0; j < impactTimePoints[i].size(); j++) {
      updateImpactTime(i, j);
    }
    calculateMinForRow(i);
  }
}

double Simulator::getInternalTime() {
  return time;
}

int Simulator::addObject(Object *object) {
  int pos = objects.size();
  objects.push_back(object);
  return pos;
}

bool Simulator::impactCircleWithSegment(Circle *circle, LineSegment *line) {
  auto v = circle->getVelocity();
  const auto &dir = line->getDirection();
  double x1 = -dir.x, y1 = -dir.y;
  double x2 = v.x, y2 = v.y;
  circle->setVelocity({(x1 * x1 * x2 + 2 * x1 * y1 * y2 - x2 * y1 * y1) / dir.squareLength(),
                       (y1 * y1 * y2 + 2 * x1 * x2 * y1 - x1 * x1 * y2) / dir.squareLength()});
  return false;
}

void Simulator::clear() {
  objects.clear();
}
