//
// Created by nikita on 14.02.18.
//
#ifndef IDEALGAS2_SIMULATOR_H
#define IDEALGAS2_SIMULATOR_H

#include "../objects/Circle.h"
#include "../objects/LineSegment.h"

using std::vector;
using std::pair;

class Simulator {
  vector<vector<double>> impactTimePoints;
  vector<double> nearestImpactInRow;
  vector<int> nearestImpactObjectInRow;
  vector<int> lastImpacted;
  vector<double> timeOfLastImpact;
  double nearestImpact = -1;
  int nearestImpactRow = -1;
  vector<Object *> objects;
  double time = 0;


  void printError(pair<Object *, Object *> objects);

  pair<Object *, Object *> getObjects(int objectNum1, int objectNum2);


  bool impactCircles(Circle *first, Circle *second);

  bool impactCircleWithSegment(Circle *circle, LineSegment *line);

  void impact(pair<Object *, Object *> objects);

  double getDistanceBetweenSegmentAndPoint(LineSegment *segment, const Vector2 &point);


  double getDistanceBetweenCircles(Circle *first, Circle *second);

  double getDistanceBetweenCircleAndSegment(Circle *circle, LineSegment *segment);

  double getDistance(pair<Object *, Object *> objects);


  double getTimeOfImpactCircles(Circle *first, Circle *second);

  double getTimeOfImpactCircleWithSegment(Circle *circle, LineSegment *segment);

  double getTimeOfImpact(pair<Object *, Object *> objects);


  void calculateTimesOfImpactsForObject(int objectNum);

  void calculateMinForRow(int objectNum);

  void calculateNearestImpact();


  void updateMin(double currentVal, int currentPos, double &min, int &minPos);

  void updateImpactTime(int i, int j);

  void updateImpactTimeForRow(int objectNum);


  void skipTimeTo(double timePoint);


  void checkIfCorrect();

public:
  int addObject(Object *object);

  void update(double deltaTime);

  void simulateTo(double deltaTime);

  void prepareSimulation();

  double getInternalTime();

  void clear();
};

#endif //IDEALGAS2_SIMULATOR_H