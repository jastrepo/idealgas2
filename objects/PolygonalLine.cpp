//
// Created by nikita on 13.03.18.
//

#include "PolygonalLine.h"

PolygonalLine::PolygonalLine() {
  type = 4;
}

PolygonalLine::PolygonalLine(const Vector2 &position) : Object(position) {
  type = 4;
}

void PolygonalLine::addPoint(const Vector2 &nextPoint, sf::Color color) {
  if (vertexes.size() < 2) {
    vertexes.emplace_back(nextPoint, color);
  } else {
    vertexes.push_back(vertexes.back());
    vertexes.emplace_back(nextPoint, color);
  }
}

void PolygonalLine::setLastPoint(const Vector2 &lastPoint, sf::Color color) {
  if (!vertexes.empty()) {
    vertexes.back() = {lastPoint, color};
  }
}

void PolygonalLine::setLastPoint(const Vector2 &lastPoint) {
  if (!vertexes.empty()) {
    vertexes.back().position = lastPoint;
  }
}

void PolygonalLine::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.draw(vertexes.data(), vertexes.size(), sf::Lines, states);
}

void PolygonalLine::addToSimulator(Simulator &simulator) {
  if (vertexes.size() > 1) {
    for (auto i = vertexes.begin(); i != vertexes.end(); i += 2) {
      simulator.addObject(new LineSegment(i->position, (i + 1)->position));
    }
  }
}
