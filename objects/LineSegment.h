//
// Created by nikita on 01.03.18.
//
#pragma once

#include "Object.h"

using sf::Vertex;

class LineSegment : public Object {
  Vector2 second, direction;
  Vertex points[2];
public:
  static const int SEGMENT = 3;

  LineSegment(const Vector2 &first, const Vector2 &second);

  [[nodiscard]] Vector2 getSecondVertex() const;

  [[nodiscard]] Vector2 getDirection() const;

  void draw(RenderTarget &target, RenderStates states) const override;
};