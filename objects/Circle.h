//
// Created by nikita on 14.02.18.
//
#pragma once

#include "Object.h"

using sf::Color;
using sf::CircleShape;

class Circle : public Object {
  const double radius;
  CircleShape circle;
public:
  static const int CIRCLE = 1;

  Circle();

  explicit Circle(double radius);

  Circle(const Vector2 &center, double radius);

  double getRadius() const;

  void setColor(const Color &newColor);

  void setPosition(const Vector2 &newPosition) override;

  void draw(RenderTarget &target, RenderStates states) const override;
};