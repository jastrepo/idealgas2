//
// Created by nikita on 22.02.18.
//

#include "Object.h"

Object::Object(const Vector2 &position) : position(position), velocity() {}

Vector2 Object::getPosition() const {
  return position;
}

Vector2 Object::getVelocity() const {
  return velocity;
}

void Object::setPosition(const Vector2 &newPosition) {
  position = newPosition;
}

void Object::setVelocity(const Vector2 &newVelocity) {
  velocity = newVelocity;
  if (velocityChangeListener != nullptr) {
    velocityChangeListener();
  }
}

int Object::getType() const {
  return type;
}

void Object::afterVelocityChange(function<void()> listener) {
  velocityChangeListener = move(listener);
}

void Object::afterImpact(function<void(const Object *)> listener) {
  impactListener = move(listener);
}

void Object::impact(const Object *object) {
  if (impactListener != nullptr) {
    impactListener(object);
  }
}
