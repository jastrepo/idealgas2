//
// Created by nikita on 22.02.18.
//
#pragma once

#include "../core/Vector2.h"

using std::function;
using sf::RenderStates;
using sf::RenderTarget;

class Object {
protected:
  Vector2 position, velocity;
  function<void()> velocityChangeListener = nullptr;
  function<void(const Object *)> impactListener = nullptr;
  int type = 0;
public:
  Object() = default;

  explicit Object(const Vector2 &position);

  [[nodiscard]] Vector2 getPosition() const;

  [[nodiscard]] Vector2 getVelocity() const;

  virtual void setPosition(const Vector2 &newPosition);

  void setVelocity(const Vector2 &newVelocity);

  [[nodiscard]] int getType() const;

  void afterVelocityChange(function<void()> listener);

  void afterImpact(function<void(const Object *)> listener);

  void impact(const Object *object);

  virtual void draw(RenderTarget &target, RenderStates states) const = 0;
};