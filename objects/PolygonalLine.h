//
// Created by nikita on 13.03.18.
//
#pragma once

#include "../simulator/Simulator.h"

class PolygonalLine : public Object {
  vector<Vertex> vertexes;
public:
  PolygonalLine();

  explicit PolygonalLine(const Vector2 &position);

  void addPoint(const Vector2 &nextPoint, Color color = Color::Black);

  void setLastPoint(const Vector2 &lastPoint, Color color);

  void setLastPoint(const Vector2 &lastPoint);

  void draw(RenderTarget &target, RenderStates states) const override;

  void addToSimulator(Simulator &simulator);
};