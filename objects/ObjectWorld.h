//
// Created by nikita on 02.03.18.
//
#pragma once

#include "Object.h"
#include <set>

using sf::Drawable;
using sf::Transformable;
using std::set;
using std::shared_ptr;

class ObjectWorld : public Drawable, public Transformable {
  set<shared_ptr<Object>> objects;
public:
  void addObject(const shared_ptr<Object> &object);

  void removeObject(const shared_ptr<Object> &object);

  void clear();

  void draw(RenderTarget &target, RenderStates states) const override;
};