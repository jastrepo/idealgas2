//
// Created by nikita on 14.02.18.
//
#include "Circle.h"

Circle::Circle() : radius(0), circle(static_cast<float>(radius)) {
  type = CIRCLE;
}

Circle::Circle(double radius) : radius(radius), circle(static_cast<float>(radius)) {
  type = CIRCLE;
}

Circle::Circle(const Vector2 &center, double radius) : Object(center), radius(radius),
                                                       circle(static_cast<float>(radius)) {
  type = CIRCLE;
}

double Circle::getRadius() const {
  return radius;
}

void Circle::setPosition(const Vector2 &newPosition) {
  Object::setPosition(newPosition);
  circle.setPosition(newPosition - radius);
}

void Circle::setColor(const Color &newColor) {
  circle.setFillColor(newColor);
}

void Circle::draw(RenderTarget &target, RenderStates states) const {
  target.draw(circle, states);
}
