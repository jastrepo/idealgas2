//
// Created by nikita on 02.03.18.
//
#include "ObjectWorld.h"

void ObjectWorld::addObject(const shared_ptr<Object> &object) {
  objects.insert(object);
}

void ObjectWorld::removeObject(const shared_ptr<Object> &object) {
  objects.erase(object);
}

void ObjectWorld::clear() {
  objects.clear();
}

void ObjectWorld::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  states.transform *= getTransform();
  for (const auto &obj: objects) {
    obj->draw(target, states);
  }
}

