//
// Created by nikita on 01.03.18.
//
#include "LineSegment.h"

LineSegment::LineSegment(const Vector2 &first, const Vector2 &second) : Object(first), second(second),
                                                                        direction(second - first) {
  points[0] = {first};
  points[1] = {second};
  type = SEGMENT;
}

Vector2 LineSegment::getSecondVertex() const {
  return second;
}

Vector2 LineSegment::getDirection() const {
  return direction;
}

void LineSegment::draw(RenderTarget &target, RenderStates states) const {
  target.draw(points, 2, sf::Lines, states);
}