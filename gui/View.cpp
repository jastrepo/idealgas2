//
// Created by nikita on 12/1/19.
//
#include "View.h"

// Functions to generate Label objects with white text
shared_ptr<tgui::Label> createLabel(const sf::String &text = "") {
  auto newLabel = tgui::Label::create(text);

  newLabel->getRenderer()->setTextColor(sf::Color::White);

  return newLabel;
}

shared_ptr<tgui::Label> createCentredLabel(const sf::String &text) {
  auto newLabel = createLabel(text);

  newLabel->setHorizontalAlignment(tgui::Label::HorizontalAlignment::Center);

  return newLabel;
}


shared_ptr<tgui::Canvas> View::createObjectWorldCanvas() {
  objectWorldCanvas = tgui::Canvas::create();

  return objectWorldCanvas;
}

shared_ptr<tgui::Button> View::createRecordVideoButton() {
  auto recordVideoButton = tgui::Button::create("Record video");

  recordVideoButton->connect(recordVideoButton->onClick.getName(), &Model::recordVideo, &model);

  return recordVideoButton;
}

shared_ptr<tgui::EditBox> View::createSecondsEditBox() {
  secondsInVideo = tgui::EditBox::create();

  secondsInVideo->setInputValidator(R"([0-9]|([1-9][0-9]*))");

  return secondsInVideo;
}

shared_ptr<tgui::EditBox> View::createVideoFilenameEditBox() {
  videoFilename = tgui::EditBox::create();

  return videoFilename;
}

shared_ptr<tgui::VerticalLayout> View::createRecordVideoLayout() {
  auto recordVideoLayout = tgui::VerticalLayout::create();

  recordVideoLayout->add(createCentredLabel("Video"));
  recordVideoLayout->add(createLabel("Video filename:"));
  recordVideoLayout->add(createVideoFilenameEditBox());
  recordVideoLayout->add(createLabel("Seconds:"));
  recordVideoLayout->add(createSecondsEditBox());
  recordVideoLayout->add(createRecordVideoButton());

  return recordVideoLayout;
}

shared_ptr<tgui::Button> View::createTakeScreenshotButton() {
  auto takeScreenshotButton = tgui::Button::create("Take screenshot");

  takeScreenshotButton->connect(takeScreenshotButton->onClick.getName(), &Model::takeScreenshot, &model);

  return takeScreenshotButton;
}

shared_ptr<tgui::EditBox> View::createScreenshotFilenameEditBox() {
  screenshotFilename = tgui::EditBox::create();

  return screenshotFilename;
}

shared_ptr<tgui::VerticalLayout> View::createTakeScreenshotLayout() {
  auto takeScreenshotLayout = tgui::VerticalLayout::create();

  takeScreenshotLayout->add(createCentredLabel("Screenshot"));
  takeScreenshotLayout->add(createLabel("Screenshot filename:"));
  takeScreenshotLayout->add(createScreenshotFilenameEditBox());
  takeScreenshotLayout->add(createTakeScreenshotButton());

  return takeScreenshotLayout;
}

shared_ptr<tgui::VerticalLayout> View::createFilesWorkLayout() {
  auto filesWorkLayout = tgui::VerticalLayout::create();

  filesWorkLayout->add(createTakeScreenshotLayout());
  filesWorkLayout->add(createRecordVideoLayout());

  return filesWorkLayout;
}

shared_ptr<tgui::Button> View::createSimulationRunButton() {
  simulationRunButton = tgui::Button::create("Start simulation");

  simulationRunButton->connect(simulationRunButton->onClick.getName(), &Model::inverseSimulation, &model);

  return simulationRunButton;
}

shared_ptr<tgui::Slider> View::createTargetFPSSlider() {
  targetFPSSlider = tgui::Slider::create(1, 60);

  targetFPSSlider->connect(targetFPSSlider->onValueChange.getName(), [this](float a) {
    model.targetFPS = size_t(a);
  });

  return targetFPSSlider;
}

shared_ptr<tgui::HorizontalLayout> View::createTargetFPSLayout() {
  auto targetFPSLayout = tgui::HorizontalLayout::create();

  targetFPSLayout->add(createLabel("Target FPS"), 0.8);
  targetFPSLayout->add(createTargetFPSSlider());

  return targetFPSLayout;
}

shared_ptr<tgui::Slider> View::createSimulationStepSlider() {
  simulationStepSlider = tgui::Slider::create(0, 100);

  simulationStepSlider->setStep(.01);
  simulationStepSlider->connect(simulationStepSlider->onValueChange.getName(), [this](float a) {
    model.simulationStep = a;
  });

  return simulationStepSlider;
}

shared_ptr<tgui::HorizontalLayout> View::createSimulationStepLayout() {
  auto simulationStepLayout = tgui::HorizontalLayout::create();

  simulationStepLayout->add(createLabel("Simulation step"), 0.8);
  simulationStepLayout->add(createSimulationStepSlider());

  return simulationStepLayout;
}

shared_ptr<tgui::Label> View::createFramesRenderedInLastSecondLabel() {
  framesRenderedInLastSecondLabel = createLabel();

  return framesRenderedInLastSecondLabel;
}

shared_ptr<tgui::Label> View::createCurrentFPSLabel() {
  currentFPSLabel = createLabel();

  return currentFPSLabel;
}

shared_ptr<tgui::VerticalLayout> View::createSimulatorSettingsLayout() {
  auto simulatorSettingsLayout = tgui::VerticalLayout::create();

  simulatorSettingsLayout->add(createCentredLabel("Simulator settings"), 0.5);
  simulatorSettingsLayout->add(createCurrentFPSLabel(), 0.5);
  simulatorSettingsLayout->add(createFramesRenderedInLastSecondLabel(), 0.7);
  simulatorSettingsLayout->add(createSimulationStepLayout());
  simulatorSettingsLayout->add(createTargetFPSLayout());
  simulatorSettingsLayout->add(createSimulationRunButton());

  return simulatorSettingsLayout;
}

shared_ptr<tgui::Button> View::createStartDrawingBoundsButton() {
  startDrawingBoundsButton = tgui::Button::create("Start drawing bounds");

  startDrawingBoundsButton->connect(startDrawingBoundsButton->onClick.getName(), &Model::startDrawingBounds, &model);

  return startDrawingBoundsButton;
}

shared_ptr<tgui::Button> View::createRejectDrawnBoundsButton() {
  rejectDrawnBoundsButton = tgui::Button::create("Reject bounds");

  rejectDrawnBoundsButton->connect(rejectDrawnBoundsButton->onClick.getName(), &Model::rejectDrawnBounds, &model);

  return rejectDrawnBoundsButton;
}

shared_ptr<tgui::Button> View::createAcceptDrawnBoundsButton() {
  acceptDrawnBoundsButton = tgui::Button::create("Accept bounds");

  acceptDrawnBoundsButton->connect(acceptDrawnBoundsButton->onClick.getName(), &Model::acceptDrawnBounds, &model);

  return acceptDrawnBoundsButton;
}

shared_ptr<tgui::HorizontalLayout> View::createDrawingBoundsLayout() {
  drawingBoundsLayout = tgui::HorizontalLayout::create();

  createAcceptDrawnBoundsButton();
  createRejectDrawnBoundsButton();

  drawingBoundsLayout->add(createStartDrawingBoundsButton());

  return drawingBoundsLayout;
}

shared_ptr<tgui::Button> View::createGenerateObjectWorldButton() {
  generateObjectWorldButton = tgui::Button::create("Generate object world");

  generateObjectWorldButton->connect(generateObjectWorldButton->onClick.getName(), &Model::generateObjectWorld, &model);

  return generateObjectWorldButton;
}

shared_ptr<tgui::VerticalLayout> View::createObjectWorldSettingsLayout() {
  auto objectWorldSettingsLayout = tgui::VerticalLayout::create();

  objectWorldSettingsLayout->add(createCentredLabel("Object world settings"), 0.5);
  objectWorldSettingsLayout->add(createGenerateObjectWorldButton());
  objectWorldSettingsLayout->add(createDrawingBoundsLayout());

  return objectWorldSettingsLayout;
}

shared_ptr<tgui::HorizontalLayout> View::createSettingsLayout() {
  auto settingsLayout = tgui::HorizontalLayout::create();

  settingsLayout->add(createObjectWorldSettingsLayout());
  settingsLayout->add(createSimulatorSettingsLayout());
  settingsLayout->add(createFilesWorkLayout());

  return settingsLayout;
}

shared_ptr<tgui::VerticalLayout> View::createAllLayout() {
  auto allLayout = tgui::VerticalLayout::create();

  allLayout->add(createSettingsLayout(), 3);
  allLayout->add(createObjectWorldCanvas(), 7);

  return allLayout;
}

Vector2 View::getCoordinateOfMouseRelativeToObjectWorld(Vector2 coords) {
  coords -= objectWorldCanvas->getPosition();
  coords -= model.getObjectWorld().getPosition();
  coords /= model.getObjectWorld().getScale();
  return coords;
}

void View::processEvents() {
  sf::Event event{};
  while (window->pollEvent(event)) {
    handleEvent(event);
    gui->handleEvent(event);
  }
}

void View::handleEvent(sf::Event event) {
  if (event.type == sf::Event::Closed ||
      (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)) {
    window->close();
  } else if (event.type == sf::Event::Resized) {
    auto windowSize = Vector2(event.size.width, event.size.height);
    auto view = window->getView();
    view.setSize(windowSize);
    view.setCenter(windowSize / 2);
    gui->setView(view);
    window->setView(view);
  } else if (event.type == sf::Event::MouseButtonReleased || event.type == sf::Event::LostFocus ||
             event.type == sf::Event::MouseLeft) {
    model.finishCurrentLine();
  } else if (event.type == sf::Event::MouseMoved) {
    Vector2 coords = window->mapPixelToCoords(sf::Mouse::getPosition(*window));
    if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && objectWorldCanvas->mouseOnWidget(coords)) {
      model.addPointToCurrentLine(getCoordinateOfMouseRelativeToObjectWorld(coords));
    } else {
      model.finishCurrentLine();
    }
  } else if (event.type == sf::Event::MouseButtonPressed) {
    Vector2 coords = window->mapPixelToCoords(sf::Mouse::getPosition(*window));
    if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && objectWorldCanvas->mouseOnWidget(coords)) {
      model.addPointToCurrentLine(getCoordinateOfMouseRelativeToObjectWorld(coords));
    }
  }
}

void View::drawGUI() {
  window->clear();
  gui->draw();
  window->display();
}

void View::drawObjectWorld() {
  objectWorldCanvas->clear();
  objectWorldCanvas->draw(model.getObjectWorld());
  objectWorldCanvas->display();
}

void View::initStartValues() {
  simulationStepSlider->setValue(10);
  targetFPSSlider->setValue(24);
}

void View::setCurrentFPS(size_t fps) {
  currentFPSLabel->setText("Current FPS: " + to_string(fps));
}

void View::setFramesRenderedInLastSecond(size_t frames) {
  framesRenderedInLastSecondLabel->setText("Frames rendered in last second: " + to_string(frames));
}

View::View() : model(*this) {
  window = make_unique<sf::RenderWindow>(sf::VideoMode(640, 640), "My window");
  gui = make_unique<tgui::Gui>(*window);

  gui->add(createAllLayout());

  initStartValues();
}

View::~View() {
  if (window->isOpen()) {
    window->close();
  }
}

void View::run() {
  while (window->isOpen()) {
    model.update();

    setCurrentFPS(model.currentFPS);
    setFramesRenderedInLastSecond(model.framesRenderedInLastSecond);

    processEvents();
    drawGUI();
    drawObjectWorld();
  }
}

void View::simulationStopped() {
  simulationRunButton->setText("Start simulation");
}

void View::simulationStarted() {
  simulationRunButton->setText("Stop simulation");
}

void View::drawingStopped() {
  drawingBoundsLayout->removeAllWidgets();
  drawingBoundsLayout->add(startDrawingBoundsButton);
}

void View::drawingStarted() {
  drawingBoundsLayout->removeAllWidgets();
  drawingBoundsLayout->add(acceptDrawnBoundsButton);
  drawingBoundsLayout->add(rejectDrawnBoundsButton);
}

void View::worldGenerated() {
  generateObjectWorldButton->setText("Reset object world");
}

string View::getScreenshotFilename() const {
  return screenshotFilename->getText();
}

string View::getVideoFilename() const {
  return videoFilename->getText();
}

size_t View::getSecondsInVideo() const {
  return stoull(secondsInVideo->getText().toAnsiString());
}
