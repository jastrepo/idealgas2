//
// Created by nikita on 12/1/19.
//
#include <random>
#include "opencv2/opencv.hpp"
#include "View.h"
#include "Model.h"

using std::random_device;
using std::uniform_real_distribution;
using std::mt19937;

// Functions to randomly generate objects
random_device &genRandomDevice() {
  static random_device randomDevice;
  return randomDevice;
}

mt19937 &genEngine() {
  static mt19937 engine(genRandomDevice()());
  return engine;
}

vector<shared_ptr<Circle>> genRandomGrid(size_t numOfCirclesInRow, double radius, double minVelocity,
                                         double maxVelocity, sf::Color circleColor = sf::Color::White,
                                         double gridSize = 1) {
  auto &engine = genEngine();
  size_t circlesNum = numOfCirclesInRow * numOfCirclesInRow, circlesAdded = 0;
  double cellSize = gridSize / numOfCirclesInRow;
  uniform_real_distribution<double> positionDistribution(-.5 + radius / cellSize, .5 - radius / cellSize),
      velocityDistribution(minVelocity, maxVelocity);
  vector<shared_ptr<Circle>> circles(circlesNum);
  for (size_t i = 0; i < numOfCirclesInRow; ++i) {
    for (size_t j = 0; j < numOfCirclesInRow; ++j) {
      auto currentCircle = circles[circlesAdded] = make_shared<Circle>(radius);
      currentCircle->setPosition(Vector2(i + positionDistribution(engine),
                                         j + positionDistribution(engine)) * cellSize + cellSize / 2);
      currentCircle->setVelocity({velocityDistribution(engine), velocityDistribution(engine)});
      currentCircle->setColor(circleColor);
      ++circlesAdded;
    }
  }
  return circles;
}


bool Model::canStartDrawingBounds() {
  return isWorldGenerated && !drawingBounds;
}

bool Model::canStopDrawingBounds() {
  return drawingBounds;
}

bool Model::stopDrawingBounds() {
  if (canStopDrawingBounds()) {
    drawingBounds = false;

    startRendering();

    return true;
  } else {
    return false;
  }
}

bool Model::canStartRendering() {
  return isWorldGenerated && !drawingBounds && simulate && !rendering;
}

bool Model::startRendering() {
  if (canStartRendering()) {
    rendering = true;

    now = system_clock::now();
    lastFrameTime = now;
    nextFrameTime = now + milliseconds(1000 / targetFPS);
    nextSecond = now + 1s;

    framesRenderedInCurrentSecond = 0;

    currentFPS = 0;
    framesRenderedInLastSecond = 0;

    return true;
  } else {
    return false;
  }
}

bool Model::canStopRendering() {
  return rendering;
}

bool Model::stopRendering() {
  if (canStopRendering()) {
    rendering = false;

    currentFPS = 0;
    framesRenderedInLastSecond = 0;

    return true;
  } else {
    return false;
  }
}

bool Model::canStartSimulating() {
  return isWorldGenerated && !drawingBounds;
}

bool Model::canTakeScreenshot() {
  return isWorldGenerated && !drawingBounds && !takingScreenshot;
}

bool Model::canRecordVideo() {
  return isWorldGenerated && !drawingBounds && !recordingVideo;
}

Model::Model(View &view) : view(view) {}

void Model::update() {
  if (rendering) {
    now = system_clock::now();

    if (now >= nextFrameTime) {
      double deltaTime = (now - lastFrameTime).count() * 1e-9;

      simulator.simulateTo(simulationStep * deltaTime);

      currentFPS = size_t(1 / deltaTime);
      ++framesRenderedInCurrentSecond;

      lastFrameTime = now;
      nextFrameTime = now + nanoseconds(size_t(1e+9 / targetFPS));
    }

    if (now >= nextSecond) {
      framesRenderedInLastSecond = framesRenderedInCurrentSecond;
      framesRenderedInCurrentSecond = 0;

      nextSecond = now + 1s;
    }
  }
}

void Model::generateObjectWorld() {
  simulate = false;
  view.simulationStopped();

  stopDrawingBounds();
  view.drawingStopped();

  stopRendering();

  objectWorld.clear();
  simulator.clear();
  const vector<shared_ptr<Circle>> &circles = genRandomGrid(9, .01, -0.01, 0.01);
  for (const auto &circle : circles) {
    objectWorld.addObject(circle);
    simulator.addObject(circle.get());
  }

  auto line = make_shared<LineSegment>(Vector2(0, 0), Vector2(1.5, 0));
  objectWorld.addObject(line);
  simulator.addObject(line.get());
  line = make_shared<LineSegment>(Vector2(0, 0), Vector2(0, 1));
  objectWorld.addObject(line);
  simulator.addObject(line.get());
  line = make_shared<LineSegment>(Vector2(1.5, 1), Vector2(1.5, 0));
  objectWorld.addObject(line);
  simulator.addObject(line.get());
  line = make_shared<LineSegment>(Vector2(1.5, 1), Vector2(0, 1));
  objectWorld.addObject(line);
  simulator.addObject(line.get());

  objectWorld.setScale(350, 350);
  objectWorld.setPosition(50, 50);
  simulator.prepareSimulation();

  if (!isWorldGenerated) {
    isWorldGenerated = true;
    view.worldGenerated();
  }
}

ObjectWorld &Model::getObjectWorld() {
  return objectWorld;
}

bool Model::inverseSimulation() {
  if (simulate) {
    simulate = false;

    stopRendering();
    view.simulationStopped();

    return true;
  } else {
    if (canStartSimulating()) {
      simulate = true;

      startRendering();
      view.simulationStarted();

      return true;
    } else {
      return false;
    }
  }
}

bool Model::startDrawingBounds() {
  if (canStartDrawingBounds()) {
    drawingBounds = true;

    drawingLine = false;
    lines.clear();

    stopRendering();
    view.drawingStarted();

    return true;
  } else {
    return false;
  }
}

void Model::addPointToCurrentLine(Vector2 vector2) {
  if (drawingBounds) {
    if (!drawingLine) {
      lines.push_back(make_shared<PolygonalLine>());
      objectWorld.addObject(lines.back());

      drawingLine = true;
    }

    lines.back()->addPoint(vector2, sf::Color::Red);
  }
}

void Model::finishCurrentLine() {
  drawingLine = false;
}

bool Model::acceptDrawnBounds() {
  if (drawingBounds) {
    for (const auto &line:lines) {
      line->addToSimulator(simulator);
    }
    simulator.prepareSimulation();

    stopDrawingBounds();
    view.drawingStopped();

    return true;
  } else {
    return false;
  }
}

bool Model::rejectDrawnBounds() {
  if (drawingBounds) {
    for (const auto &line:lines) {
      objectWorld.removeObject(line);
    }

    stopDrawingBounds();
    view.drawingStopped();

    return true;
  } else {
    return false;
  }
}

void Model::takeScreenshot() {
  if (canTakeScreenshot()) {
    const string &filename = view.getScreenshotFilename();
    if (filename.empty()) {
      return;
    }

    takingScreenshot = true;
    stopRendering();

    auto size = Vector2(objectWorld.getPosition()) * 2 + objectWorld.getScale();

    sf::RenderTexture t;
    t.create(uint(size.x), uint(size.y));

    t.clear();
    t.draw(objectWorld);
    t.display();

    t.getTexture().copyToImage().saveToFile(filename + ".png");

    startRendering();
    takingScreenshot = false;
  }
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"

void Model::recordVideo() {
  if (canRecordVideo()) {
    const string &filename = view.getVideoFilename();
    if (filename.empty()) {
      return;
    }

    size_t seconds = view.getSecondsInVideo();

    recordingVideo = true;
    stopRendering();

    auto size = Vector2(objectWorld.getPosition()) * 2 + objectWorld.getScale();

    cv::VideoWriter video(filename + ".mp4", cv::VideoWriter::fourcc('m', 'p', '4', 'v'), targetFPS,
                          cv::Size(size.y, size.x));

    sf::RenderTexture t;
    t.create(uint(size.x), uint(size.y));

    for (size_t i = 0; i < seconds * targetFPS; ++i) {
      simulator.simulateTo(simulationStep / targetFPS);

      t.clear();
      t.draw(objectWorld);
      t.display();

      auto pixelsPtr = t.getTexture().copyToImage().getPixelsPtr();

      cv::Mat frame(uint(size.x), uint(size.y), CV_8UC3);
      for (size_t x = 0; x < size.x; ++x) {
        for (size_t y = 0; y < size.y; ++y) {
          auto &pixel = frame.at<cv::Vec3b>(int(x), int(y));
          pixel[2] = *pixelsPtr;
          ++pixelsPtr;
          pixel[1] = *pixelsPtr;
          ++pixelsPtr;
          pixel[0] = *pixelsPtr;
          ++pixelsPtr;
          ++pixelsPtr;
        }
      }

      video << frame;
    }

    video.release();

    startRendering();
    recordingVideo = false;
  }
}

#pragma clang diagnostic pop
