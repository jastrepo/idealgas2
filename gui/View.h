//
// Created by nikita on 12/1/19.
//
#pragma once

#include "Model.h"

using std::unique_ptr;
using std::make_unique;
using std::to_string;

class View {
  // Unique pointers to GUI objects
  unique_ptr<sf::RenderWindow> window;
  unique_ptr<tgui::Gui> gui;

  // Pointers to parts of GUI
  shared_ptr<tgui::Button> generateObjectWorldButton;
  shared_ptr<tgui::Canvas> objectWorldCanvas;
  shared_ptr<tgui::HorizontalLayout> drawingBoundsLayout;
  shared_ptr<tgui::Button> startDrawingBoundsButton, acceptDrawnBoundsButton, rejectDrawnBoundsButton;
  shared_ptr<tgui::Label> currentFPSLabel, framesRenderedInLastSecondLabel;
  shared_ptr<tgui::Slider> simulationStepSlider, targetFPSSlider;
  shared_ptr<tgui::Button> simulationRunButton;
  shared_ptr<tgui::EditBox> screenshotFilename;
  shared_ptr<tgui::EditBox> videoFilename, secondsInVideo;

  // Internal model, which controls simulation
  Model model;

  // Functions for constructing GUI

  shared_ptr<tgui::Canvas> createObjectWorldCanvas();

  shared_ptr<tgui::Button> createRecordVideoButton();

  shared_ptr<tgui::EditBox> createSecondsEditBox();

  shared_ptr<tgui::EditBox> createVideoFilenameEditBox();

  shared_ptr<tgui::VerticalLayout> createRecordVideoLayout();

  shared_ptr<tgui::Button> createTakeScreenshotButton();

  shared_ptr<tgui::EditBox> createScreenshotFilenameEditBox();

  shared_ptr<tgui::VerticalLayout> createTakeScreenshotLayout();

  shared_ptr<tgui::VerticalLayout> createFilesWorkLayout();

  shared_ptr<tgui::Button> createSimulationRunButton();

  shared_ptr<tgui::Slider> createTargetFPSSlider();

  shared_ptr<tgui::HorizontalLayout> createTargetFPSLayout();

  shared_ptr<tgui::Slider> createSimulationStepSlider();

  shared_ptr<tgui::HorizontalLayout> createSimulationStepLayout();

  shared_ptr<tgui::Label> createFramesRenderedInLastSecondLabel();

  shared_ptr<tgui::Label> createCurrentFPSLabel();

  shared_ptr<tgui::VerticalLayout> createSimulatorSettingsLayout();

  shared_ptr<tgui::Button> createStartDrawingBoundsButton();

  shared_ptr<tgui::Button> createRejectDrawnBoundsButton();

  shared_ptr<tgui::Button> createAcceptDrawnBoundsButton();

  shared_ptr<tgui::HorizontalLayout> createDrawingBoundsLayout();

  shared_ptr<tgui::Button> createGenerateObjectWorldButton();

  shared_ptr<tgui::VerticalLayout> createObjectWorldSettingsLayout();

  shared_ptr<tgui::HorizontalLayout> createSettingsLayout();

  shared_ptr<tgui::VerticalLayout> createAllLayout();

  // Accessory functions

  Vector2 getCoordinateOfMouseRelativeToObjectWorld(Vector2 coords);

  void initStartValues();

  // Function for drawing and controlling GUI

  void processEvents();

  void handleEvent(sf::Event event);

  void drawGUI();

  void drawObjectWorld();

  // Private functions to manipulate GUI for the View object

  void setCurrentFPS(size_t fps);

  void setFramesRenderedInLastSecond(size_t frames);

public:

  View();

  ~View();

  void run();

  // Public functions to manipulate GUI from the Model object

  void simulationStopped();

  void simulationStarted();

  void drawingStopped();

  void drawingStarted();

  void worldGenerated();

  // Functions to get input from GUI

  string getScreenshotFilename() const;

  string getVideoFilename() const;

  size_t getSecondsInVideo() const;
};