//
// Created by nikita on 12/1/19.
//
#pragma once

#include "../objects/ObjectWorld.h"
#include "../objects/PolygonalLine.h"
#include <chrono>

using std::chrono::system_clock;
using std::string;
using std::chrono::nanoseconds;
using std::operator ""s;
using std::chrono::milliseconds;
using std::make_shared;

class View;

class Model {
  // GUI view, which this Model object controls
  View &view;

  // Variables for objects and their physical simulation
  ObjectWorld objectWorld;
  Simulator simulator;

  // Variables of internal state of Model object
  bool simulate = false;
  bool drawingBounds = false, isWorldGenerated = false, rendering = false;

  // Variables for drawing bounds
  bool drawingLine = false;
  vector<shared_ptr<PolygonalLine>> lines;

  // Variables for rendering
  system_clock::time_point now, lastFrameTime, nextFrameTime, nextSecond;
  size_t framesRenderedInCurrentSecond = 0;

  // Variables to do file works
  bool takingScreenshot = false, recordingVideo = false;

  // Functions to change internal state of Model object and to check if it is possible

  bool canStartDrawingBounds();

  bool canStopDrawingBounds();

  bool stopDrawingBounds();

  bool canStartRendering();

  bool startRendering();

  bool canStopRendering();

  bool stopRendering();

  bool canStartSimulating();

  bool canTakeScreenshot();

  bool canRecordVideo();

public:
  // Public variables for writing - there is no need for setter
  double simulationStep = 0;
  size_t targetFPS = 1;

  // Public variables for reading - there is no need for getter
  size_t currentFPS = 0, framesRenderedInLastSecond = 0;

  explicit Model(View &view);

  void update();

  // Function to initialization objects and simulator

  void generateObjectWorld();

  ObjectWorld &getObjectWorld();

  // Functions to start and stop rendering

  bool inverseSimulation();

  // Function to start and stop drawing bounds

  bool startDrawingBounds();

  void addPointToCurrentLine(Vector2 vector2);

  void finishCurrentLine();

  bool acceptDrawnBounds();

  bool rejectDrawnBounds();

  // Function to do file works

  void takeScreenshot();

  void recordVideo();
};