//
// Created by nikita on 14.02.18.
//
#pragma once
#pragma clang diagnostic push
#pragma ide diagnostic ignored "google-explicit-constructor"

#include <TGUI/TGUI.hpp>

class Vector2 {
public:
  double x, y;

  Vector2();

  Vector2(double x, double y);

  Vector2(const sf::Vector2f &second);

  Vector2(const tgui::Vector2f &second);

  void operator+=(const Vector2 &second);

  void operator-=(const Vector2 &second);

  void operator/=(const Vector2 &second);

  void operator+=(const double &second);

  void operator-=(const double &second);

  void operator*=(const double &second);

  Vector2 operator+(const Vector2 &second) const;

  Vector2 operator-(const Vector2 &second) const;

  Vector2 operator/(const Vector2 &second) const;

  Vector2 operator+(const double &second) const;

  Vector2 operator-(const double &second) const;

  Vector2 operator*(const double &second) const;

  Vector2 operator/(const double &second) const;

  [[nodiscard]] double squareLength() const;

  [[nodiscard]] double getSquareDistanceTo(const Vector2 &second) const;

  [[nodiscard]] double getDistanceTo(const Vector2 &second) const;

  operator sf::Vector2f() const;

  operator tgui::Vector2f() const;
};

#pragma clang diagnostic pop