//
// Created by nikita on 14.02.18.
//
#include <cmath>
#include "Vector2.h"

Vector2::Vector2() : x(0), y(0) {}

Vector2::Vector2(double x, double y) : x(x), y(y) {}

Vector2::Vector2(const sf::Vector2f &second) : x(second.x), y(second.y) {}

Vector2::Vector2(const tgui::Vector2f &second) : x(second.x), y(second.y) {}

void Vector2::operator+=(const Vector2 &second) {
  x += second.x;
  y += second.y;
}

void Vector2::operator-=(const Vector2 &second) {
  x -= second.x;
  y -= second.y;
}

void Vector2::operator/=(const Vector2 &second) {
  x /= second.x;
  y /= second.y;
}

void Vector2::operator+=(const double &second) {
  x += second;
  y += second;
}

void Vector2::operator-=(const double &second) {
  x -= second;
  y -= second;
}

void Vector2::operator*=(const double &second) {
  x *= second;
  y *= second;
}

Vector2 Vector2::operator+(const Vector2 &second) const {
  return {x + second.x, y + second.y};
}

Vector2 Vector2::operator-(const Vector2 &second) const {
  return {x - second.x, y - second.y};
}

Vector2 Vector2::operator/(const Vector2 &second) const {
  return {x / second.x, y / second.y};
}

Vector2 Vector2::operator+(const double &second) const {
  return {x + second, y + second};
}

Vector2 Vector2::operator-(const double &second) const {
  return {x - second, y - second};
}

Vector2 Vector2::operator*(const double &second) const {
  return {x * second, y * second};
}

Vector2 Vector2::operator/(const double &second) const {
  return {x / second, y / second};
}

double Vector2::squareLength() const {
  return x * x + y * y;
}

double Vector2::getSquareDistanceTo(const Vector2 &second) const {
  return (x - second.x) * (x - second.x) + (y - second.y) * (y - second.y);
}

double Vector2::getDistanceTo(const Vector2 &second) const {
  return sqrt(getSquareDistanceTo(second));
}

Vector2::operator sf::Vector2f() const {
  return {static_cast<float>(x), static_cast<float>(y)};
}

Vector2::operator tgui::Vector2f() const {
  return {static_cast<float>(x), static_cast<float>(y)};
}
